package sematec.weatherproject.Weather;

/**
 * Created by sabere_pc on 07/02/2018.
 */

public class PublicMethods {


    public static String convertFtoC(String fStr) {
        double f = Double.parseDouble(fStr);
        double c = (f - 32) / 1.8;
        return (int)c  + "°C";
    }

}
