package sematec.weatherproject.Weather;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import sematec.weatherproject.R;
import sematec.weatherproject.Weather.Models.Condition;
import sematec.weatherproject.Weather.Models.YahooModel;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener {
    EditText city;
    TextView result;
    ProgressDialog dialog;
    ListView forecastList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_weather );

        city = findViewById ( R.id.city );
        result = findViewById ( R.id.result );
        forecastList = findViewById ( R.id.forecastList );
        dialog = new ProgressDialog ( this );
        findViewById ( R.id.show ).setOnClickListener ( this );


    }

    @Override
    public void onClick(View view) {
        String cityValue = city.getText ().toString ();
        getDataFromYahoo ( cityValue );

    }

    private void getDataFromYahoo(String cityValue) {
        dialog.show ();
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"
                + cityValue +
                "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client = new AsyncHttpClient ();
        client.get ( url, new TextHttpResponseHandler () {
            @Override

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText ( WeatherActivity.this, "Faild", Toast.LENGTH_SHORT ).show ();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseData ( responseString );
            }

            @Override
            public void onFinish() {
                super.onFinish ();
                dialog.dismiss ();
            }
        } );

    }

    private void parseData(String responseString) {
        Gson gson = new Gson ();
        YahooModel yahoo = gson.fromJson ( responseString, YahooModel.class );
        String resultStr = yahoo.getQuery ().getResults ().getChannel ().getItem ().getCondition ().getTemp ();
        result.setText ( city.getText ().toString () + " : " + resultStr );


        result.setText ( city.getText ().toString () + " : " + PublicMethods.convertFtoC ( resultStr ) );

        //forecast list
        ForeCastListAdapter adapter = new ForeCastListAdapter ( this,
                yahoo.getQuery ().getResults ().getChannel ().getItem ().getForecast () );
        forecastList.setAdapter ( adapter );


    }
}
