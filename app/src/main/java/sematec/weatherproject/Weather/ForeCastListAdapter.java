package sematec.weatherproject.Weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import sematec.weatherproject.R;
import sematec.weatherproject.Weather.Models.Forecast;

/**
 * Created by sabere_pc on 07/02/2018.
 */

public class ForeCastListAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forecasts;

    public ForeCastListAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }



    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int i) {
        return forecasts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.forecast_list_item, null);



        TextView day = v.findViewById( R.id.day);
        TextView date = v.findViewById(R.id.date);
        TextView high = v.findViewById(R.id.high);
        TextView low = v.findViewById(R.id.low);
        TextView text = v.findViewById(R.id.text);
        ImageView img = v.findViewById(R.id.img) ;
        Forecast thisValue = forecasts.get(i);


        day.setText(thisValue.getDay());
        date.setText(thisValue.getDate());
        high.setText(PublicMethods.convertFtoC(thisValue.getHigh()));
        low.setText(PublicMethods.convertFtoC(thisValue.getLow()));
        text.setText(thisValue.getText());


        if( thisValue.getText().toLowerCase().contains("sun"))
            Picasso.with(mContext).load(R.drawable.sunny).into(img);

        if( thisValue.getText().toLowerCase().contains("cloud"))
            Picasso.with(mContext).load(R.drawable.cloudy).into(img);

        if( thisValue.getText().toLowerCase().contains("rain"))
            Picasso.with(mContext).load(R.drawable.rainy).into(img);

        if( thisValue.getText().toLowerCase().contains("snow"))
            Picasso.with(mContext).load(R.drawable.snow).into(img);



        return v;
    }
}
